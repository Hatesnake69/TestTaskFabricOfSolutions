from typing import List

from sqlalchemy import update
from sqlalchemy.future import select
from sqlalchemy.orm import Session

from db.models import User, CreateUserCommand, UpdateUserCommand


class UserDAL:
    def __init__(self, db_session: Session):
        self.db_session = db_session

    async def create_user(self, cmd: CreateUserCommand):
        new_user = User(
            name=cmd.name,
            password=cmd.password)
        self.db_session.add(new_user)
        await self.db_session.flush()

    async def get_all_users(self) -> List[User]:
        q = await self.db_session.execute(select(User).order_by(User.id))
        return q.scalars().all()

    async def update_user(self, cmd: UpdateUserCommand):
        q = update(User).where(User.id == cmd.id)
        if cmd.name:
            q = q.values(name=cmd.name)
        if cmd.password:
            q = q.values(password=cmd.password)
            q = q.values(updated_at=cmd.updated_at)
        q.execution_options(synchronize_session="fetch")
        await self.db_session.execute(q)
