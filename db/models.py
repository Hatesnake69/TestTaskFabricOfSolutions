import datetime

from pydantic import BaseModel
from sqlalchemy import Column, Integer, String, DateTime, ForeignKey

from db import Base


class User(Base):
    __tablename__ = 'users'

    id = Column(Integer, primary_key=True)
    name = Column(String, nullable=False, unique=True)
    password = Column(String, nullable=False)
    created_at = Column(DateTime(timezone=True), default=datetime.datetime.now())
    updated_at = Column(DateTime(timezone=True), onupdate=datetime.datetime.now())


class MailingList(Base):
    __tablename__ = 'mailing_lists'

    id = Column(Integer, primary_key=True)
    started_at = Column(DateTime(timezone=True))
    ended_at = Column(DateTime(timezone=True))
    message_text = Column(String, nullable=False)
    filter = Column(String)


class Client(Base):
    __tablename__ = 'clients'

    id = Column(Integer, primary_key=True)
    phone_number = Column(String, nullable=False)
    mobile_operator_code = Column(String, nullable=False)
    tag = Column(String)
    timezone = Column(String, nullable=False)


class Message(Base):
    __tablename__ = 'messages'

    id = Column(Integer, primary_key=True)
    created_at = Column(DateTime(timezone=True), default=datetime.datetime.now())
    status = Column(String, nullable=False)
    mailing_list_id = Column(Integer, ForeignKey('mailing_lists.id'), nullable=False)
    client_id = Column(Integer, ForeignKey('clients.id'), nullable=False)


class CreateUserCommand(BaseModel):
    name: str
    password: str


class UpdateUserCommand(BaseModel):
    id: int
    name: str
    password: str
    updated_at: datetime.datetime = datetime.datetime.now()
